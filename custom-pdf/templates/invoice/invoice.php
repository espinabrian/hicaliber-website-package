<style>

    a {
        color: #f46e08;
        text-decoration: none;
    }
    .container {
        font-size: 12px
    }
    table {
        border-collapse: collapse; 
        font-size: 12px
    }

    table th {
        text-align: left;
        background: #cecece;
    }

    .sm-txt {
        font-size: 10px
    }

    .highlight {
        background: #cecece;
    }

    table th,
    table td {
        padding: 8px 5px;
        border: 1px solid #000;
    }

    .container {
        padding : 20px 40px;
    }

    .right {
        text-align: right;
    }

    .terms {        
        font-size: 12px;
    }
    
    .ul-no-margin {
        margin-left: 20px;
    }
    .ul-no-margin ul {
        margin: 0px;
    }
    

</style>


<?php



function hic_invoice_get_packages_value( $selected = "", $options = [] ){
    $data = [];
    if($options) {
        
        foreach( $options as $opt ) {
            if( $opt['title']."|".$opt['set_up_fee'] ==  $selected )  {
                $data = $opt;
                break;    
            }
        }
    }
    return $data;
}

function hic_invoice_get_design_value( $seleced = "", $options = [] ){
    $data = [];
    if($options) {
        
        foreach( $options as $opt ) {
            if( $opt['title'] ==  $seleced )  {
                $data = $opt;
                break;    
            }
        }
    }
    return $data;
}


function hic_invoice_money($number) {
    return "$".  number_format((float)$number, 2, '.', '');
}


function hic_invoice_get_selected_add_ons( $acf_add_ons = [] , $selected_add_ons = [] ) {
    
    $data = [];
    foreach( $selected_add_ons as $o ){
	    foreach( $acf_add_ons as $x ) {
	        if( trim( $x['title'] ) == trim( $o )  ){
	            $data[] = $x;
	        }
	    }
	 }
    
    return $data;
}
	
	
	 


?>

<div class="container">

    <p class="right"><?php echo date( 'j M Y' , strtotime( current_time( 'mysql' ) ) ) ; ?></p>

    <?php 
    
        $fname = explode(" ", $args['name']);
        $packages = get_field( 'sf_packages' , 'option' );
        $design = get_field( 'sf_design' , 'option' );
        $add_ons = get_field( 'sf_add_ons' , 'option' ); 
        
        $add_ons_gf = explode(", ",$args['add_ons']);
        
        $package_selected = hic_invoice_get_packages_value( $args['package'] , $packages );
        $design_selected = hic_invoice_get_design_value( $args['design'] , $design );
        
        $per_month = 0;
        $total_amount = 0;
        
        if( $add_ons_gf ) {
            $add_on_selected = hic_invoice_get_selected_add_ons(  $add_ons , $add_ons_gf );    
        }
        
        
        // Display the INTRO text
        echo str_replace( '{{first_name}}' ,$fname[0], get_field('sf_introduction','option') ); 
        
    ?>
    
        <table width="100%">         
    <?php
    
        foreach( $args['other_detail'] as $key => $detail ) {
            if( $detail ) {
                echo "<tr>";
                echo "<th width='30%'>{$key}</td><td width='70%'>{$detail}</td>";
                echo "</tr>";    
            }
            
        }
    
    ?>
    </table>
    
    <div>&nbsp;</div>
    
    <table width="100%">
        <thead>
            <tr>
                <th>ITEMS</th>
                <th>FEE</th>
            </tr>
        </thead>
        <tbody>
            
            
            <?php if( $package_selected  ) : 
                $per_month += $package_selected['per_month_price'];
                $total_amount += $package_selected['set_up_fee'];
            ?>
            <tr>
                <td>
                    <b>Package: <?php echo $package_selected['title']; ?></b>
                    
                    <?php
                    /*
                    <div class="ul-no-margin">
                    <ul><li><b><?php echo hic_invoice_money( $package_selected['per_month_price'] ) . "</b> " . $package_selected['frequency'] ?></li></ul>
                    <?php echo $package_selected['description']; ?>
                    </div>
                    <?php */ ?>
                    <div>
                        <b>Design Selected: <?php echo $design_selected['title']; ?></b>
                    <div>    
                    <?php if( isset( $package_selected['set_up_fee_desc'] ) ) : ?>
                        <div><?php echo $package_selected['set_up_fee_desc']; ?></div>
                    <?php endif; ?>
                </td>
                <td><?php echo hic_invoice_money( $package_selected['set_up_fee'] )?></td>
            </tr>
            
            <?php if( $package_selected['per_month_price'] ) : ?>
            <tr>
                <td><?php echo $package_selected['recurring_fee_desc'] ?></td>
                <td><?php echo hic_invoice_money($package_selected['per_month_price']) . " " . $package_selected['frequency']  ?></td>
            </tr>    
            <?php endif; ?>
            
            <?php endif; ?>
            
            
            
            
            <?php if( $add_on_selected ): ?>
            
            <tr>
                <td class="highlight" colspan="2"><b>ADD ONS</b></td>
            </tr>
            
            
                <?php foreach( $add_on_selected as $add_on ) :
                
                if( $add_on['price'] == "" ) { $add_on['price'] = 0; }
                
                $per_month += $add_on['price'];  ?>
                
                    <tr>
                        <td>
                            <?php echo $add_on['title'];  ?>              
                        </td> 
                        <td>
                            <?php if( $add_on['price']  ) : ?>
                                <?php echo hic_invoice_money($add_on['price']); ?> <span class="sm-txt">per month</span>
                            <?php else: ?>
                                FREE
                            <?php endif; ?>
                        </td>
                    </tr>
                
                <?php endforeach; ?>
            
            <?php endif; ?>

            <tr>
                <td class="right"><b>Total Monthly Fee</b></td>
                <td><b> <?php echo hic_invoice_money($per_month); ?> </b></td>
            </tr>             
            <tr>
                <td class="right"><b>Setup / Upfront Fee</b></td>
                <td><b><?php echo hic_invoice_money($total_amount); ?></b></td>
            </tr>            
        </tbody>
    </table>
    
    <?php echo get_field('sf_signature','option') ?>

    <pagebreak>
        <?php echo get_field('sf_terms_and_conditions','option') ?>
    </pagebreak>

</div>