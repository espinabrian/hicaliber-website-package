<style>
    .container, table {
        color: #829E95;
        font-family: 'Open Sans';
        font-size: 12px;
    }
    
	.item-header {
		padding: 9px 10px;
		font-size: 20px;
		background: #f5f6f5;
		color: #829E95;
	}
	.item-header .title {
		display: inline-block;
    	margin-left: 20px;
    }
    .item-content {
        background: #fbfbfb;
        padding: 20px;
    }
    
    .left {
        float: left;
        width: 30%;
    }

    .right {
        float: right;
        width: 70%;
    }

    .content-inner {
        padding-left: 30px;
    }

    .address {
        font-weight: bold;
        margin-bottom: 15px;
    }

    .width-half {
        width: 50%;
    }

    .width-full {
        width: 100%;
    }
    
</style>


<div class='container'>
<?php

    function content_width_50( $data = "", $prefix = "" ) {

        $html = "";

        if( $data ) {
            $html .= "<div style='margin-bottom: 5px'>"; 
            $html .= "<table style='width:100%'>";
                $html .= "<tr>";
                    $html .= "<td style='width: 100px' valign='top'>".$prefix."</td>";
                    $html .= "<td>".$data."</td>";
                $html .= "</tr>";
            $html .= "</table>";
            $html .= "</div>";
        }

        return $html;

    }

    function left_img( $img = ""  ) {
        return "<div class='img' style='height: 150px;width: 230px;background-position: center;background-repeat: no-repeat;background-image:url(".$img.")' ></div>";
    }

    function description( $desc = "" ) {
        if( $desc != "" ) {
            return filter_var( $desc, FILTER_SANITIZE_STRING);
        }
    }


    $a = json_decode( stripslashes( $args ) , true );
    
    $html = "";
    $az_range = range('A','Z');
    for( $i = 0 ; $i <= (sizeof($a) - 1); $i++ ) {
        $trip = $a[$i];
        $html .= "<div class='item-header'>".$az_range[$i].". <span class='title'>".$trip['title']."</span></div>";	
        $html .= "<div class='item-content'>";
        $html .= "<div class='item-content-inner'>";
            $html .= "<div class='left'>";
                $html .= left_img( $trip['img'] );
            $html .= "</div>";
            $html .= "<div class='right'><div class='content-inner'>";
                $html .= "<div class='address'>".$trip['address']."</div>";
                $html .= "<div style='margin-bottom: 10px'>".description($trip['desc'])."...</div>";	

                    $html .= "<div>";                        
                        $html .= content_width_50( $trip['contact_no'] , "Contact No: " );
                        $html .= content_width_50( $trip['hours'] , "Hours: " );
                        $html .= content_width_50( $trip['website'] , "Website: " );
                        $html .= '<div style="clear: both; margin: 0pt; padding: 0pt; "></div>';
                    $html .= "</div>";

            $html .= "</div></div>";
            $html .= '<div style="clear: both; margin: 0pt; padding: 0pt; "></div>';
        $html .= "</div>";
        $html .= "</div>";
    }
    echo $html;
?>
</div>

