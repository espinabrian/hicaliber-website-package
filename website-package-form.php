<?php
   /*
   Plugin Name: Hicaliber Website Package
   Version: 1.0
   Author: Hicaliber
   License: GPL2
   Description: Multiform Plugin for Website packages
   */
?>
<?php

function edump( $s = "" ) {
    echo "<pre>";
    var_dump( $s );
    echo "</pre>";
}

function gf_getFormByName($name) {
    $forms = GFAPI::get_forms();
    foreach ($forms as $key => $form) {
        if($form['title'] === $name){
            return $form;
        }
    }
    return false;
}


$form = gf_getFormByName('Website Packages');

require_once('website-option-fields.php');
require_once('form-export.php');
require_once('invoice-pdf.php');

add_filter( 'gform_pre_render_' . $form["id"], 'website_package_update_option_fields' );

function label_up_date( $data = [] ) {

	$html = "";

		$html .= "<div class='hic-box text-center'><div class='hic-content'>";
		$html .= "<div class='hic-title c'>".$data['title']."</div>";
		
		$html .= "<div class='hic-blurb'>";
		    
		    if( isset( $data['per_month_price'] ) && $data['per_month_price'] != "" ) {
		        $html .= "<h2><span class='dollar-sign'>$</span>{$data['per_month_price']}<span class='total-price-text'>{$data['frequency']}</span></h2>";    
		    } else {
		        $html .= "<h2><span class='dollar-sign'>$</span>{$data['set_up_fee']}<span class='total-price-text'>Total Price</span></h2>";    
		    }
		    
		    
		    $html .= $data['description'];
		    
		    $html .= '<div class="hic-button-wrap"><span class="button">Select</span></div>';
                            
		$html .= "</div>";
		
		$html .= "<div><p class='pricing-notes'><em>{$data['disclaimer']}</em></p></div>";
		$html .= "</div></div>";

	return $html;
}

function label_up_date_2( $data = [] ) {

	$html = "";

		$html .= "<div class='hic-box text-center'><div class='hic-content'>";
		$html .= "<div class='hic-title c'>".$data['title']."</div>";
		
		$html .= "<div class='info-desc'>{$data['desc']}</div>";
		
		$html .= "<div class='price-desc'>{$data['price_desc']}</div>";
		
		if( isset($data['footer_desc']) && $data['footer_desc'] != '' ) {
		    $html .= '<p class="pricing-notes"><em>'.$data['footer_desc'].'</em></p>';    
		}
		
		
		$html .= "</div></div>";

	return $html;
}

function design_label_update( $d ) {
    
    $html = "";
    $html .= "<div class='preview-design'><div class='inner'>";
    $html .= "<div class='title c'>{$d['title']}</div>";
    $html .= "<div class='bg-helper preview-img' style='background-position: top center; background-image:url({$d['preview_image']})'></div>";
    $html .= "<div class='footer'><a href='{$d['preview']}' target='_blank'>Preview</a></div>";
    $html .= "</div></div>";
    
    return $html;
}

function large_check_box( $data ) {
    
    $html = "";
    
    $html .= "<div class='lg-checkbox-wrap'>";
    $html .= "<div class='title'>{$data['title']}</div>";
    $html .= "<div class='desc'>{$data['description']}</div>";
    $html .= "</div>";
    
    return $html;
}

function website_package_update_option_fields ( $form ){
	
	// edump( $packages );
	
	$sample_design = get_field('page_content_builder', 1867 );
	$d = get_field('page_content_builder', 2525 );
	$pricing = [];
	$design = [];
	$choices = [];
	
	$packages = get_field( 'sf_packages' , 'option' );
	$design = get_field( 'sf_design' , 'option' );
	$add_ons = get_field( 'sf_add_ons' , 'option' ); 
	$intro = get_field( 'sf_introduction' , 'option' ); 
        
    $page = get_the_ID();

     foreach( $form['fields'] as &$field )  {
    
        if( $field->id == 40 ) {            
            if( $packages ) {
                if( is_array( $packages ) ) {
                    foreach( $packages as $opt) {                        

                        if( $opt['form_location']  ) {
                            if( in_array( $page, $opt['form_location'] ) ) {
                                $choices[] = array( 'text' => label_up_date( $opt ), 'value' => $opt['title'], 'price' => $opt['set_up_fee'] );
                            }                            
                        } else {
                            $choices[] = array( 'text' => label_up_date( $opt ), 'value' => $opt['title'], 'price' => $opt['set_up_fee'] );
                        }
                        
    				    
    			    }
    			    // check how many option add class style number of options
    			    $field->cssClass .= " number-of-options-".sizeof( $choices );
                    $field->choices = $choices; 
                }
            }
		   
        }
        
         if( $field->id == 38 && $add_ons) {
			foreach( $add_ons as $opt) {	
                if( $opt['form_location_add_on']  ) {
                    if( in_array( $page, $opt['form_location_add_on'] ) ) {
                        $choices_add_ons[] = array( 'text' => large_check_box( $opt ), 'value' => $opt['title'] );
                    }
                } else {
                    $choices_add_ons[] = array( 'text' => large_check_box( $opt ), 'value' => $opt['title'] );
                }
			}
            $field->choices = $choices_add_ons;    
        }
        
        if( $field->id == 3 && $design) {
            foreach( $design as $opt) {	
				$choices_design[] = array( 'text' => design_label_update($opt) , 'value' => $opt['title'] );
			}
            $field->choices = $choices_design;
        }
        
		
        
    }
    return $form;
}


/*************************
 * Invoice notification
**************************/


function attach_pdf_invoice( $notification, $form, $entry ){
    
    if( $notification['name'] == 'Invoice'){
        
        // get add on value
        $field_id_addons        = 38;
        $field_addons           = GFFormsModel::get_field( $form, $field_id_addons );
        $field_value_addons     = is_object( $field_addons ) ? $field_addons->get_value_export( $entry ) : '';
        
        $post_data = [
            'name'          => rgar( $entry, '13' ),
            'package'       => rgar( $entry, '40' ),
            'design'        => rgar( $entry, '3' ),
            'add_ons'       => $field_value_addons,
            'other_detail'  => [
                'Company / Business Name'   => rgar( $entry, '6' ),
                'ABN'                       => rgar( $entry, '12' ),
                'Contact Name'              => rgar( $entry, '13' ),
                'Phone Number'              => rgar( $entry, '14' ),
                'Email Address'             => rgar( $entry, '8' ),
                'Physical Address'          => rgar( $entry, '35' ),
                'Website Domain Name'       => rgar( $entry, '16' ),
            ]
        ];
        
        $file = INVOICE_PDF::generate_pdf( 'invoice' , 'Order_Confirmation' , $post_data , "F" );
        if( $file ) {
            $notification['attachments'][] = $file;    
        }
        
    }
    
    return $notification;
}


add_filter( 'gform_notification_' . $form["id"], 'attach_pdf_invoice', 10, 3 );


if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Form Settings',
		'menu_title'	=> 'Sign Up Form Settings',
		'menu_slug' 	=> 'sign-up-form-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
}

 
/****************************
 * Add Customer to stripe
 *****************************/ 
 
add_filter( 'gform_stripe_customer_id', function ( $customer_id, $feed, $entry, $form ) {
    if ( rgars( $feed, 'meta/transactionType' ) == 'product' && rgars( $feed, 'meta/feedName' ) == 'Stripe Feed 1' ) {
        
        $customer_meta = array();
 
        $metadata_field = rgars( $feed, 'meta/metaData' );
        
        foreach ($metadata_field as $metadata ) {
            
            $meta_value = $metadata['value'];
            
            if ($metadata['custom_key'] == 'Email') {
                if( $meta_value && strtolower( $meta_value ) !== 'do not send receipt' )  {
                    $customer_meta['email'] = gf_stripe()->get_field_value( $form, $entry, $meta_value );
                }
            }

            if ($metadata['custom_key'] == 'Name') {
                if( $meta_value && strtolower( $meta_value ) !== 'do not send receipt' )  {
                    $customer_meta['name'] = gf_stripe()->get_field_value( $form, $entry, $meta_value );
                }
            }            
            
        }
        
 
        $customer = gf_stripe()->create_customer( $customer_meta, $feed, $entry, $form );
 
        return $customer->id;
    }
 
    return $customer_id;
}, 10, 4 );

function wpse_load_plugin_css() {
    $plugin_url = plugin_dir_url( __FILE__ );

    wp_enqueue_style( 'style', $plugin_url . 'hic-website-package.css' );
}
add_action( 'wp_enqueue_scripts', 'wpse_load_plugin_css' );
add_filter( 'gform_init_scripts_footer', '__return_true' );
// add_filter( 'gform_stripe_connect_enabled', '__return_false' );