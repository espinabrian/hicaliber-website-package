<?php

class INVOICE_PDF {

    // Plugin name
    const PLUGIN_NAME = 'hicaliber-website-package';

    // Plugin Folder
    const PLUGIN_FOLDER = ABSPATH . 'wp-content/plugins/'.self::PLUGIN_NAME;

    // Load MPDF
    const MDPDF_LIB = self::PLUGIN_FOLDER.'/custom-pdf/vendor/autoload.php';

    // save the pdf here !! important to create the folder
    const ARCHIVE_FOLDER = ABSPATH . 'wp-content/uploads/'.self::PLUGIN_NAME;

    public static function money_figure( $amt = "" ){
        if( $amt ) {
            $amt = (float) $amt;
            $amt = number_format( $amt, 2 );
            return '$'.$amt;
        }
    }

    public static function sample_admin_notice__error() {
        ?>
        <div class="notice notice-error">
            <p><b><?php _e( 'Error notice!', 'custom-pdf' ); ?></b></p>
            <p><b><?php _e( 'PDF Library is not included please contact the web admin', 'custom-pdf' ); ?></b></p>
        </div>
        <?php
    }

    public static function assets_folder( $template_name = "" ) {

        if( $template_name ) {
            return self::PLUGIN_FOLDER . "/custom-pdf/templates/" .    $template_name . "/" .$template_name . ".php"; 
        }
    }
    
    
    public static function footer( $data ) {
        
        $html = "";
    
            $txt_color = $data['text_color'] ? $data['text_color'] : '#000000';
            $bg_color = $data['background_color'] ? $data['background_color'] : '#FFFFFF';
            
            $html .= "<table style='vertical-align: bottom; border-collapse: collapse;' width='100%' bgcolor='".$bg_color."'>";
                $html .= "<tr>";
                    $html .= "<td style='border: none; padding:20px; color:".$txt_color."'>";
                        $html .= "<div style='padding-bottom: 20px; font-size:16px'>".$data['text']."</div>";
                        $html .= "<div>".$data['disclaimer']."</div>";
                    $html .= "</td>";
                $html .= "</tr>";
            $html .= "</table>";
            
        return  $html ;
        
    }


    public static function header( $data ) {
        
        $txt_color = $data['text_color'] ? $data['text_color'] : '#000000';
        $bg_color = $data['background_color'] ? $data['background_color'] : '#FFFFFF';
        
        $style = "padding-top: 20px; padding-bottom: 20px; border:none; height:80px;";
        
        $logo = $data['logo'] ? '<img style="width:200px; margin-left:40px" src="'.$data['logo'].'">' : '';
        $text = $data['title'] ? $data['title'] : "";
        
        $header = "<table width='100%' style='background:".$bg_color."; border-collapse: collapse; color:".$txt_color."'><tr>";
       
        $header .= "<td style='".$style."' align='left' valign='middle'>".$logo."</td>";
            if( $text ) {
                $header .= "<td style='border:none;text-align:left; font-size: 25px; padding-right:40px' align='right'  valign='middle'>{$text}</td>";
            }
       
        $header .= "</tr></table>";
        
        return $header;
    }


    /**
     *  @param  Array
     *  @return Location and file name of invoice pdf
     */
     

    public static function generate_pdf( $template = "", $file_name = "",  $args = [] , $type = "F"  ) {
        
        
        if( !file_exists( self::MDPDF_LIB ) || $template == ""  || $file_name == "" ) {
            return false;
        }

        if( $type == "D" ) {
            $pdf_output = $file_name . "_" . time() . ".pdf";
        } else {
            $pdf_output = self::ARCHIVE_FOLDER . "/" . $file_name . "_" . time() . ".pdf";
        }
        

        require_once self::MDPDF_LIB;


        $defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        ob_start();
        require_once self::assets_folder( $template );
        $html_invoice = ob_get_contents();
        ob_end_clean();

        $mpdf = new \Mpdf\Mpdf(
            [
                'fontDir' => array_merge($fontDirs, [
                    self::PLUGIN_FOLDER . '/custom-pdf/Opens_Sans/',
                ]),
                'fontdata' => $fontData + [
                    'Open Sans' => [
                        'R' => 'OpenSans-Regular.ttf',
                        'B' => 'OpenSans-Bold.ttf'
                    ]
                ],

                'default_font' => 'Open Sans',
                'setAutoTopMargin' => 'stretch',
                'setAutoBottomMargin' => 'stretch',
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_left' => 0,
                'margin_right' => 0,
                'margin_top' => 0,
                'margin_bottom' => 0,
                'margin_header' => 0,
                'margin_footer' => 0
            ]
        );

        $mpdf->SetDisplayMode('fullwidth');
        $header_option = get_field('sf_header','option');
        $footer_option = get_field('sf_footer','option');


        $footer = "";
        $header = "";
         
        if( $header_option ) {
            $header = self::header( $header_option );
            $mpdf->SetHTMLHeader($header, 'O' );
        }

        if( $footer_option ) {
            $footer = self::footer( $footer_option );
            $mpdf->SetHTMLFooter($footer, 'O' );
        }
    
        // Write Invoice page 
        $mpdf->WriteHTML( $html_invoice );
    
        if( $type == "OO") {
          $mpdf->Output();  
        } else {
            $mpdf->Output(  $pdf_output , $type );    
            return $pdf_output;
        }
        

        
    }

    public function __construct(){
        
        // checks if pdf libary is loaded
        if( !file_exists( self::MDPDF_LIB ) ) {
            add_action( 'admin_notices',  array( $this , 'sample_admin_notice__error') );
            return false;
        }
        //Check if the directory already exists.
        if(!is_dir(self::ARCHIVE_FOLDER)){
            //Directory does not exist, create directory
            mkdir(self::ARCHIVE_FOLDER, 0755, true);
        }
        
    }

}

new INVOICE_PDF();