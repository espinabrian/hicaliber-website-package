<?php 
    $json_form_export = plugin_dir_path( __FILE__ ) . '/form-export.txt' ;
    define("GF_IMPORT_FILE", $json_form_export);
    function hical_import_form() {
        if(defined("GF_IMPORT_FILE") && !get_option("gf_imported_file")){
            GFExport::import_file(GF_IMPORT_FILE);
            update_option("gf_imported_file", true);
        }
    }
    add_action('init', 'hical_import_form' );
?>